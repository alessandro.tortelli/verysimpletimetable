package com.verysimplesoftware.alessandro.verysimpletimetable;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

 public class AdapterScadenzeNotificate extends ArrayAdapter<Scadenza> {

        public AdapterScadenzeNotificate(Context context, int textViewResourceId,
                                           List<Scadenza> objects) {
            super(context, textViewResourceId, objects);
        }

        @Override
        public View getView(int position, View convertView, final ViewGroup parent) {
            try {
            /*if (convertView != null) {
                Logger.getAnonymousLogger().log(Level.INFO, "chiamato getview con convertView != null");
                return convertView;
            }*/
                LayoutInflater inflater = (LayoutInflater) getContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.listitem_notificate, null);
                final TextView descrizione = (TextView) convertView.findViewById(R.id.description);
                final TextView date = (TextView) convertView.findViewById(R.id.date);
                //DatePicker dp = convertView.findViewById(R.id.datePicker);
                //TimePicker tp = convertView.findViewById(R.id.timePicker);final Scadenza scadenza = getItem(position);
                final Scadenza scadenza = getItem(position);
                descrizione.setText(scadenza.getDescrizione());
                SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd" );
                date.setText(dateFormatter.format(scadenza.getScadenza().getTime()));
                return convertView;
            } catch (Exception ex) {
                Logger.getAnonymousLogger().log(Level.SEVERE, ex.getMessage());
                ex.printStackTrace();
            }
            return convertView;
        }
}
