package com.verysimplesoftware.alessandro.verysimpletimetable;

import java.util.Calendar;

public class Scadenza {
    private String descrizione;

    public Calendar getScadenza() {
        return scadenza;
    }

    public void setScadenza(Calendar scadenza) {
        this.scadenza = scadenza;
    }

    private Calendar scadenza;

    public Scadenza(Scadenza scadenza)
    {
        this.descrizione = new String(scadenza.getDescrizione());
        this.scadenza = (Calendar) scadenza.getScadenza().clone();
        this.setNotified(scadenza.isNotified());


    }
    public Scadenza(String descrizione,Calendar scadenza,boolean notified) {
        this.descrizione = descrizione;
        this.scadenza = scadenza;
        this.notified = notified;
    }


    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public boolean isNotified() {
        return notified;
    }

    public void setNotified(boolean notified) {
        this.notified = notified;
    }

    private boolean notified;
}

