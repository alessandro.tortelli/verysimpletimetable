package com.verysimplesoftware.alessandro.verysimpletimetable;


import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class AdapterScadenzeDaNotificare extends ArrayAdapter<Scadenza> {

    public AdapterScadenzeDaNotificare(Context context, int textViewResourceId,
                                       List<Scadenza> objects) {
        super(context, textViewResourceId, objects);
    }

    private AlertDialog.Builder setupPopupForDescription(final Scadenza scadenza, final TextView descrizione) throws Exception {
        Logger.getAnonymousLogger().log(Level.INFO, "chiamata setupPopupForDescription " + scadenza.getDescrizione());
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        String title;
        final EditText input = new EditText(getContext());
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        /*
        if (input.getParent()!=null){
            ((ViewGroup)input.getParent()).removeView(input);
        }
        */
        builder.setView(input);

        //title=Resources.getSystem().getString(R.string.modify_description);
        title = getContext().getString(R.string.modify_description);
        //title = "description";
        input.setText(scadenza.getDescrizione());
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                scadenza.setDescrizione(input.getText().toString());
                descrizione.setText(scadenza.getDescrizione());
                dialog.dismiss();
                //dialog.cancel();

            }
        });

        builder.setTitle(title);

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return builder;
    }
    public AlertDialog.Builder setupPopupForNewDescription() throws Exception {
        Logger.getAnonymousLogger().log(Level.INFO, "chiamata setupPopupForNewDescription ");
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        String title;
        final EditText input = new EditText(getContext());
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        /*
        if (input.getParent()!=null){
            ((ViewGroup)input.getParent()).removeView(input);
        }
        */
        builder.setView(input);

        //title=Resources.getSystem().getString(R.string.modify_description);
        title = getContext().getString(R.string.insert_description);
        //title = "description";
        input.setText("");
        final Scadenza scadenza = new Scadenza("",Calendar.getInstance(),false);

        final AlertDialog.Builder pfnd = setupPopupForNewDate(scadenza);
        final AlertDialog dialogForNewDate = pfnd.create();
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                scadenza.setDescrizione(input.getText().toString());
                //descrizione.setText(scadenza.getDescrizione());
                dialog.dismiss();
                dialogForNewDate.show();
                //dialog.cancel();

            }
        });

        builder.setTitle(title);

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return builder;
    }
    private AlertDialog.Builder setupPopupForDate(final Scadenza scadenza, final TextView textData) throws Exception {
        Logger.getAnonymousLogger().log(Level.INFO, "chiamata setupPopupForDate " + scadenza.getDescrizione());
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        String title;
        final  DatePicker datePicker = new DatePicker(getContext());
        builder.setView(datePicker);


        title = getContext().getString(R.string.modify_description);
        datePicker.updateDate(scadenza.getScadenza().get(Calendar.YEAR),
                scadenza.getScadenza().get(Calendar.MONTH),
                scadenza.getScadenza().get(Calendar.DAY_OF_MONTH));
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                scadenza.getScadenza().set(datePicker.getYear(),datePicker.getMonth(),datePicker.getDayOfMonth());
                SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd" );
                textData.setText(dateFormatter.format(scadenza.getScadenza().getTime()));
                //textData.setText(scadenza.getScadenza().getTime().toString());
                dialog.dismiss();
                //dialog.cancel();

            }
        });

        builder.setTitle(title);

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return builder;
    }
    private AlertDialog.Builder setupPopupForNewDate(final Scadenza scadenza) throws Exception {
        Logger.getAnonymousLogger().log(Level.INFO, "chiamata setupPopupForDate " + scadenza.getDescrizione());
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        String title;
        final  DatePicker datePicker = new DatePicker(getContext());
        builder.setView(datePicker);


        title = getContext().getString(R.string.insert_date);
        datePicker.updateDate(scadenza.getScadenza().get(Calendar.YEAR),
                scadenza.getScadenza().get(Calendar.MONTH),
                scadenza.getScadenza().get(Calendar.DAY_OF_MONTH));
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                scadenza.getScadenza().set(datePicker.getYear(),datePicker.getMonth(),datePicker.getDayOfMonth());
                add(new Scadenza(scadenza));
                dialog.dismiss();
                //dialog.cancel();

            }
        });

        builder.setTitle(title);

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return builder;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        try {
            /*if (convertView != null) {
                Logger.getAnonymousLogger().log(Level.INFO, "chiamato getview con convertView != null");
                return convertView;
            }*/
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listitem, null);
            final TextView descrizione = (TextView) convertView.findViewById(R.id.description);
            final TextView date = (TextView) convertView.findViewById(R.id.date);
            //DatePicker dp = convertView.findViewById(R.id.datePicker);
            //TimePicker tp = convertView.findViewById(R.id.timePicker);final Scadenza scadenza = getItem(position);
            final Scadenza scadenza = getItem(position);
            descrizione.setText(scadenza.getDescrizione());
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd" );
            date.setText(dateFormatter.format(scadenza.getScadenza().getTime()));

            //description
            final Button buttonForDescription = convertView.findViewById(R.id.descriptionButton);
            final AlertDialog.Builder adbForDescription = setupPopupForDescription(scadenza, descrizione);
            final AlertDialog dialogForDescription = adbForDescription.create();
            buttonForDescription.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    // Code here executes on main thread after user presses button
                    Logger.getAnonymousLogger().log(Level.INFO, "Cliccato bottone Description:" + scadenza.getDescrizione());
                    dialogForDescription.show();

                }
            });

            //date
            final Button buttonForDate = convertView.findViewById(R.id.dateButton);
            final AlertDialog.Builder adbForDate = setupPopupForDate(scadenza, date);
            final AlertDialog dialogForDate = adbForDate.create();
            buttonForDate.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    // Code here executes on main thread after user presses button
                    Logger.getAnonymousLogger().log(Level.INFO, "Cliccato bottone Description:" + scadenza.getDescrizione());
                    dialogForDate.show();

                }
            });
            final Button delete = convertView.findViewById(R.id.deleteButton);
            delete.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                    final AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                    alertDialog.setTitle(R.string.deleting_deadline);
                    alertDialog.setMessage(getContext().getResources().getString(R.string.deleting_deadline_answer));
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    alertDialog.dismiss();
                                    remove(scadenza);
                                }
                            });
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getContext().getResources().getString(R.string.cancel),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    alertDialog.dismiss();
                                }
                            });
                    alertDialog.show();

                }

            });
            return convertView;
        } catch (Exception ex) {
            Logger.getAnonymousLogger().log(Level.SEVERE, ex.getMessage());
            ex.printStackTrace();
        }
        return convertView;
    }

}
