package com.verysimplesoftware.alessandro.verysimpletimetable;

import java.util.ArrayList;
import java.util.List;

public class Scadenze {

    Scadenze(List<Scadenza> scadenze){
        this.listaScadenze = scadenze;
    }

    List<Scadenza> listaScadenze;
    public List<Scadenza> getListaScadenze() {
        return listaScadenze;
    }

    public void setListaScadenze(List<Scadenza> listaScadenze) {
        this.listaScadenze = listaScadenze;
    }


    List<Scadenza> filtByNotify(boolean notify){
        List<Scadenza> out = new ArrayList<Scadenza>();
        for (Scadenza s: listaScadenze){
            if (s.isNotified()==notify){
                out.add(s);
            }
        }
        return out;
    }
}
