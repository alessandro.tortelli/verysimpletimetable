package com.verysimplesoftware.alessandro.verysimpletimetable;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView listView = (ListView)findViewById(R.id.list);
        List<Scadenza> list = new ArrayList<>();
        Calendar c=Calendar.getInstance();
        Calendar now=Calendar.getInstance();
        c.set(2019,0,21,12,15);
        list.add(new Scadenza("Don't stand so close to me",c,false));
        list.add(new Scadenza("Love the way you lie",c,false));
        list.add(new Scadenza("L'essenziale",now,true));
        Scadenze scadenze= new Scadenze(list);
        final AdapterScadenzeDaNotificare adapter = new AdapterScadenzeDaNotificare(this, R.layout.listitem, scadenze.filtByNotify(false));
        final AdapterScadenzeNotificate adapterNotificate = new AdapterScadenzeNotificate(this,R.layout.listitem_notificate,scadenze.filtByNotify(true));
        listView.setAdapter(adapter);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    AlertDialog.Builder adb = adapter.setupPopupForNewDescription();
                    AlertDialog dialog=adb.create();
                    dialog.show();
                }catch(Exception ex){
                    ex.printStackTrace();
                }

            }
        });
    }
}